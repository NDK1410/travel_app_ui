import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import AppIntroSlider from "react-native-app-intro-slider";

const sliders = [
  {
    key: "one",
    title: "JUST TRAVEL",
    text: "pretium nibh ipsum consequat nisl vel pretium lectus quam",
    image: require("./assets/1.png"),
  },
  {
    key: "two",
    title: "TAKE A BREAK",
    text: "pretium nibh ipsum consequat nisl vel pretium lectus quam",
    image: require("./assets/2.png"),
  },
  {
    key: "three",
    title: "ENJOY YOUR JOURNEY",
    text: "pretium nibh ipsum consequat nisl vel pretium lectus quam",
    image: require("./assets/3.png"),
  },
]

export default class App extends React.Component {
  state = {showHomepage : false};
  _itemRendered = ({item}) => {
    return (
      <View style={{flex: 1}}>
        <Image
          source={item.image}
          style={styles.image}
        />
        <Text style={styles.titleText}>
          {item.title}
        </Text>
        <Text style={styles.itemText}>
          {item.text}
        </Text>
      </View>
    );
  };

  render() {
    if (this.state.showHomePage){
      return <App/>
    } else
    return (
      <AppIntroSlider
        renderItem={this._itemRendered}
        data={sliders}
        activeDotStyle={{
        backgroundColor:"#21465b",
        width:30
      }}
      />
    );
  }
}

const styles = StyleSheet.create({
  itemText: {
    paddingHorizontal: 30,
    fontSize: 15,
    color: "#b5b5b5",
    textAlign: 'center',
  },
  titleText: {
    paddingTop: 25,
    paddingBottom: 10,
    fontSize: 23,
    fontWeight: "bold",
    color: "#21465b",
    alignSelf: 'center',
  },
  image: {
    width: "100%",
    height: "73%",
    resizeMode: "cover",
  },
});
